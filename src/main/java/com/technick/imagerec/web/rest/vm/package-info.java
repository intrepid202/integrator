/**
 * View Models used by Spring MVC REST controllers.
 */
package com.technick.imagerec.web.rest.vm;
