import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { IntegratorSharedModule } from 'app/shared/shared.module';
import { IntegratorCoreModule } from 'app/core/core.module';
import { IntegratorAppRoutingModule } from './app-routing.module';
import { IntegratorHomeModule } from './home/home.module';
import { IntegratorEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    IntegratorSharedModule,
    IntegratorCoreModule,
    IntegratorHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    IntegratorEntityModule,
    IntegratorAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class IntegratorAppModule {}
